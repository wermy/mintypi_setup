#!/bin/bash

echo "Enabling SSH..."
update-rc.d ssh enable &&
invoke-rc.d ssh start
echo "SSH enabled!"

echo "Setting up fbtft..."

config_txt=/boot/config.txt
echo "Enabling SPI..."
if ! grep '^dtparam=spi=on' $config_txt; then
  echo 'dtparam=spi=on' >> $config_txt
else
  echo "SPI already enabled."
fi

etc_modules=/etc/modules
echo "Adding entries to $etc_modules..."
if ! grep '^spi-bcm2835' $etc_modules; then
  echo 'spi-bcm2835' >> $etc_modules
  echo 'fbtft_device' >> $etc_modules
else
  echo "$etc_modules already set up."
fi

fbtft_conf=/etc/modprobe.d/fbtft.conf
if [ -f $fbtft_conf ]; then
  echo "fbtft.conf exists, deleting it and creating new one..."
  rm $fbtft_conf
else
  echo "fbtft.conf does not exist, creating it..."
fi
echo 'options fbtft_device name=fb_ili9341 gpios=reset:25,dc:24 speed=80000000 bgr=1 rotate=90 custom=1 verbose=0 fps=60' >> $fbtft_conf
echo "Done."

echo "Fetching rpi-fbcp..."
git clone https://github.com/tasanakorn/rpi-fbcp
echo "Done."
cd rpi-fbcp/
mkdir build
cd build/
cmake ..
echo "Setting up fbcp..."
make
sudo install fbcp /usr/local/bin/fbcp
echo "Done."

rc_local=/etc/rc.local
echo "Adding fbcp to $rc_local"
if ! grep '^fbcp\&' $rc_local; then
    sed -i -e 's/^exit 0/fbcp\&\nexit 0/g' $rc_local
else
  echo "$rc_local already set up."
fi

echo "Configuring screen resolution..."
minty_pi_screen_section=#minty_pi_screen_settings
if ! grep '^$minty_pi_screen_section' $config_txt; then
    sed -i -e 's/^framebuffer_/#framebuffer_/g' $config_txt
    echo '$minty_pi_screen_section' >> $config_txt
    echo 'disable_overscan=1' >> $config_txt
    echo 'hdmi_cvt=640 480 60 1' >> $config_txt
    echo 'hdmi_group=2' >> $config_txt 
    echo 'hdmi_group=2' >> $config_txt
    echo 'hdmi_mode=87' >> $config_txt
    echo 'arm_freq=1000' >> $config_txt
    echo 'gpu_freq=500' >> $config_txt
    echo 'core_freq=500' >> $config_txt
    echo 'sdram_freq=500' >> $config_txt
    echo 'sdram_schmoo=0x02000020' >> $config_txt
    echo 'over_voltage=2' >> $config_txt
    echo 'sdram_over_voltage=2' >> $config_txt

else
  echo "Screen already set up."
fi

echo "Downloading Retrogame from Adafruit..."
curl -f -s -o /tmp/retrogame https://raw.githubusercontent.com/adafruit/Adafruit-Retrogame/master/retrogame
if [ $? -eq 0 ]; then
	mv /tmp/retrogame /usr/local/bin
	chmod 755 /usr/local/bin/retrogame
	echo "Done"
else
	echo "Error downloading retrogame..."
fi

retrogame_cfg=retrogame.cfg
echo "Copying $retrogame_cfg to /boot"
cp /boot/mintyPi/$retrogame_cfg  /boot/$retrogame_cfg

retrogame_rules=10-retrogame.rules
echo "Copying retrogame udev config..."
cp /boot/mintyPi/$retrogame_rules /etc/udev/rules.d/$retrogame_rules

echo "Making Retrogame launch on boot..."
if ! grep 'retrogame' $rc_local; then
    sed -i -e 's/^exit 0/\/usr\/local\/bin\/retrogame\&\nexit 0/g' $rc_local
    echo "Done."
else
  echo "Retrogame looks like it was already set to run at boot."
fi

asound_conf=asound.conf
echo "Copying USB sound config..."
cp /boot/mintyPi/$asound_conf  /etc/$asound_conf

retroarch_cfg=retroarch.cfg
echo "Copying retroarch config..."
cp "/boot/mintyPi/$retroarch_cfg" "/opt/retropie/configs/all/$retroarch_cfg" 

es_cfg=es_input.cfg
echo "Copying emulationstation config..."
cp /boot/mintyPi/$es_cfg /opt/retropie/configs/all/emulationstation/$es_cfg

echo "Installing rfkill..."
sudo apt-get install rfkill

echo "Setting up wifi/bt scripts..."
enable_wireless=enable-wireless.sh
disable_wireless=disable-wireless.sh

game_list=gamelist.xml
cp /boot/mintyPi/$enable_wireless /home/pi/RetroPie/retropiemenu/$enable_wireless
cp /boot/mintyPi/$disable_wireless /home/pi/RetroPie/retropiemenu/$disable_wireless
cp /boot/mintyPi/$game_list /home/pi/.emulationstation/gamelists/retropie/$game_list

echo "Disabling wait for network on boot..."
rm -f /etc/systemd/system/dhcpcd.service.d/wait.conf

echo "Removing samba (use sftp!)..."
sudo apt-get remove -y --purge samba

echo "Configuring git..."
echo 'raspberry' | sudo -u pi git config --global user.ghname "mintyPi User"
echo 'raspberry' | sudo -u pi git config --global user.ghemail "mintyPi@sudomod.com"

echo "Setting up mintyPi update script"
minty_update=minty-update.sh
minty_logo=minty-logo.png
cd /home/pi
echo 'raspberry' | sudo -u pi git clone https://github.com/wermy/mintyPi.git
cp /boot/mintyPi/$minty_update /home/pi/RetroPie/retropiemenu/$minty_update
cp /boot/mintyPi/$minty_logo /home/pi/RetroPie/retropiemenu/icons/$minty_logo

# echo "Setting up Hoolyhoo's battery monitor scripts..."
cd /home/pi
echo 'raspberry' | sudo -u pi git clone https://github.com/HoolyHoo/MintyComboScript.git
cd MintyComboScript/
sudo chmod 777 MintyInstall.sh
sudo ./MintyInstall.sh
cd ..

echo "Downloading custom themes..."
mkdir /home/pi/.emulationstation/themes
cd /home/pi
echo 'raspberry' | sudo -u pi git clone https://github.com/wermy/es-theme-material.git
echo 'raspberry' | sudo -u pi git clone https://github.com/wstevens0n/tft-mintypi.git
echo 'raspberry' | sudo -u pi git clone https://github.com/wermy/es-theme-carbon.git
mv es-theme-material /home/pi/.emulationstation/themes/
mv tft-mintypi /home/pi/.emulationstation/themes/
mv es-theme-carbon /home/pi/.emulationstation/themes/

rc_local=/etc/rc.local
echo "Adding mintyPi startup script to $rc_local"
if ! grep '^minty-startup\&' $rc_local; then
    sudo sed -i '/\"exit 0\"/!s/exit 0/\/home\/pi\/mintyPi\/minty-startup.sh \&\nexit 0/g' $rc_local
else
  echo "$rc_local already set up."
fi
chmod +x /home/pi/mintyPi/minty-startup.sh
sudo rm -rf /boot/mintyPi
echo "All done!"